import java.util.Calendar;
import java.util.Scanner;

public class ZadanieJeden {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Podaj imie");
        String firstName = sc.nextLine();
        System.out.println("Podaj nazwisko");
        String lastName = sc.nextLine();
        System.out.println("Podaj datę (rok) urodzenia:");
        int birthYear = sc.nextInt();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        int age = year - birthYear;

        System.out.println("Witaj " + firstName + " "+ lastName + " masz lat " +  age);
		System.out.println("Koniec programu");
        System.out.println("Miłego dnia");

    }
}
